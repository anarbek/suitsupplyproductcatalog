﻿using ProductCatalogCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductCatalogMVCApp.Models
{
    public class TempProductModel : Product
    {
        public string PhotoPath { get; set; }
    }
}