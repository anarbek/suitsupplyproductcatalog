﻿using ProductCatalogCommon.BLL;
using ProductCatalogCommon.Models;
using ProductCatalogCommon.Utilities;
using ProductCatalogMVCApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductCatalogMVCApp.Controllers
{
    public class ProductController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region Create Product
        [HttpGet]
        public ActionResult ProductCreate(bool reset = false)
        {
            TempProductModel product = new TempProductModel();
            if (!reset)
            {
                product = TempData["productToCreate"] as TempProductModel;
                RestoreProductTempData(product);
            }
            return View(product);
        }

        [HttpPost]
        public ActionResult ProductCreate(TempProductModel product)
        {
            ViewBag.Message = "";
            SetProductTempDataForCreate(product);
            if (ModelState.IsValid)
            {
                ProductBLL productRepository = new ProductBLL();
                Enums.enmFileUploadResult photoUploadResult = SetProductPhoto(product);
                if (photoUploadResult != Enums.enmFileUploadResult.Success) {
                    SetProductPhotoErrors(photoUploadResult, "Photo couldn't be uploaded");
                    return View(product);
                }
                if (product.Price > Constants.PriceToBeConfirmed)
                {
                    ViewBag.PriceShouldBeConfirmed = true;
                    return View(product);
                }
                Enums.enmObjectCreateUpdateStatus productCreateStatus = productRepository.Insert(product);
                if (productCreateStatus == Enums.enmObjectCreateUpdateStatus.Success)
                {
                    SetTempMessage("Product created succesfully");
                }
                else
                {
                    SetProductModelErrors(productCreateStatus, "Product couldn't be created");
                    return View(product);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ProductPriceConfirm(TempProductModel product)
        {
            ViewBag.Message = "";
            SetProductTempDataForCreate(product);
            if (ModelState.IsValid)
            {
                ProductBLL productRepository = new ProductBLL();
                product.Photo = product.PhotoPath;
                Enums.enmObjectCreateUpdateStatus productCreateStatus = productRepository.Insert(product);

                if (productCreateStatus == Enums.enmObjectCreateUpdateStatus.Success)
                {
                    SetTempMessage("Product created succesfully");
                }
                else
                {
                    SetProductModelErrors(productCreateStatus, "Product couldn't be created");
                    return RedirectToAction("ProductCreate", new { product = Request.RequestContext.RouteData.Values["productToCreate"] });
                }
            }
            return RedirectToAction("Index");
        } 
        #endregion

        #region Edit Product
        [HttpGet]
        public ActionResult ProductEdit(int id, bool handleErrors = false)
        {
            TempProductModel product = GetSingleProduct(id);
            if (handleErrors)
            {
                product = TempData["productToEdit"] as TempProductModel;
                RestoreProductTempData(product);
            }
            return View(product);
        }

        [HttpPost]
        public ActionResult ProductEdit(TempProductModel product)
        {
            ViewBag.Message = "";
            SetProductTempDataForEdit(product);
            if (ModelState.IsValid)
            {
                ProductBLL productRepository = new ProductBLL();
                Enums.enmFileUploadResult photoUploadResult = SetProductPhoto(product);
                if (photoUploadResult != Enums.enmFileUploadResult.Success)
                {
                    SetProductPhotoErrors(photoUploadResult, "Photo couldn't be uploaded");
                    return View(product);
                }
                if (product.Price > Constants.PriceToBeConfirmed)
                {
                    ViewBag.PriceShouldBeConfirmed = true;
                    return View(product);
                }
                Enums.enmObjectCreateUpdateStatus productEditStatus = productRepository.Update(product);
                if (productEditStatus == Enums.enmObjectCreateUpdateStatus.Success)
                {
                    SetTempMessage("Product was updated succesfully");
                    return View(product);
                }
                else
                {
                    SetProductModelErrors(productEditStatus, "Product couldn't be updated");
                    return View(product);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ProductEditPriceConfirm(TempProductModel product)
        {
            ViewBag.Message = "";
            SetProductTempDataForEdit(product);
            if (ModelState.IsValid)
            {
                ProductBLL productRepository = new ProductBLL();
                product.Photo = product.PhotoPath;
                Enums.enmObjectCreateUpdateStatus productEditStatus = productRepository.Update(product);
                if (productEditStatus == Enums.enmObjectCreateUpdateStatus.Success)
                {
                    SetTempMessage("Product updated succesfully");
                }
                else
                {
                    SetProductModelErrors(productEditStatus, "Product couldn't be updated");
                    return RedirectToAction("ProductEdit", new { product = Request.RequestContext.RouteData.Values["productToEdit"], id = product.Id, handleErrors = true });
                }
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Delete Product
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult DeleteProduct(int id)
        {
            ProductBLL productRepository = new ProductBLL();
            if (ModelState.IsValid)
            {
                Product product = GetSingleProduct(id);
                if (product == null)
                {
                    return ErrorMessage("", "Error : Product Not Found :" + id);
                }

                try
                {
                    int result = productRepository.Delete(product);
                }
                catch (Exception ex)
                {
                    return ErrorMessage("", "Exception : " + ex.ToString());
                }
                return Json(new
                {
                    valid = true,
                    Message = "Product was deleted Succesfully",
                    redirect = "None"
                });
            }
            return ErrorMessage("", "Something wen weong");
        }
        #endregion
        
        #region ModelState and Validation stuff

        private void SetProductModelErrors(Enums.enmObjectCreateUpdateStatus productCreateStatus, string defaultMessage)
        {
            switch (productCreateStatus)
            {
                case Enums.enmObjectCreateUpdateStatus.ErrorCodeDuplicate:
                    ModelState.AddModelError("Code", "Code should be unique");
                    break;
                case Enums.enmObjectCreateUpdateStatus.ErrorWrongPrice:
                    ModelState.AddModelError("Price", "Wrong price");
                    break;
                case Enums.enmObjectCreateUpdateStatus.ErrorObjectNotFound:
                    ModelState.AddModelError("ProductCreateEditStatus", "Product not found");
                    break;
                case Enums.enmObjectCreateUpdateStatus.ErrorWrongPrimaryKey:
                    ModelState.AddModelError("Id", "Wrong primary key value");
                    break;
                case Enums.enmObjectCreateUpdateStatus.ErrorDBRelated:
                    ModelState.AddModelError("ProductCreateEditStatus", "Some error occured on db side, please contact admin");
                    break;
                case Enums.enmObjectCreateUpdateStatus.ErrorOther:
                    ModelState.AddModelError("ProductCreateEditStatus", "Something went wrong");
                    break;
                default:
                    break;
            }
            SetTempErrorMessage(defaultMessage);
        }

        private void SetProductPhotoErrors(Enums.enmFileUploadResult photoUploadResult, string defaultMessage)
        {
            switch (photoUploadResult)
            {
                case Enums.enmFileUploadResult.ErrorFileSize:
                    ModelState.AddModelError("Photo", "Photo size should be less then " + Constants.MaximumFileUploadSize + " KB");
                    break;
                case Enums.enmFileUploadResult.ErrorFileExtension:
                    ModelState.AddModelError("Photo", "Only image files are allowed for uploading");
                    break;
                case Enums.enmFileUploadResult.ErrorFileUpload:
                    ModelState.AddModelError("Photo", "Photo couldn't be uploaded, please contact admin");
                    break;
                default:
                    break;
            }
            SetTempErrorMessage(defaultMessage);
        }

        private void RestoreProductTempData(TempProductModel product)
        {
            ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
        }

        private void SetProductTempDataForCreate(TempProductModel product)
        {
            TempData["productToCreate"] = product;
            TempData["ModelState"] = ModelState;
        }

        private void SetProductTempDataForEdit(TempProductModel product)
        {
            TempData["productToEdit"] = product;
            TempData["ModelState"] = ModelState;
        } 
        #endregion

        #region Photo related stuff
        private Enums.enmFileUploadResult SetProductPhoto(TempProductModel product)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    if (file.ContentLength > (Constants.MaximumFileUploadSize * 1024)) {
                        return Enums.enmFileUploadResult.ErrorFileSize;
                    }
                    var fileName = Path.GetFileName(file.FileName);
                    var supportedTypes = new[] { "jpg", "jpeg", "png", "gif", "bmp" };
                      
                    try
                    {
                        var path = Path.Combine(Server.MapPath("~/Images/Uploads/"), fileName);
                        var fileExt = System.IO.Path.GetExtension(path).Substring(1).ToLower();
                        if (!supportedTypes.Contains(fileExt))
                        {
                            return Enums.enmFileUploadResult.ErrorFileExtension;
                        }
                        file.SaveAs(path);
                        product.Photo = file.FileName;
                        product.PhotoPath = file.FileName;
                        return Enums.enmFileUploadResult.Success;
                    }
                    catch (Exception ex)
                    {
                        return Enums.enmFileUploadResult.ErrorFileUpload;
                    }                    
                }
            }
            return Enums.enmFileUploadResult.Success;
        } 
        #endregion

        private TempProductModel GetSingleProduct(int id)
        {
            ProductBLL productRepository = new ProductBLL();
            Product product = productRepository.GetProduct(id);
            TempProductModel result = new TempProductModel();
            if (product == null) return result;
            result.Code = product.Code;
            result.Name = product.Name;
            result.Price = product.Price;
            result.Photo = product.Photo;
            result.PhotoPath = product.Photo;
            result.LastUpdated = product.LastUpdated;
            return result;
        }

    }
}
