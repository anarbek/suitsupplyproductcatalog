﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductCatalogMVCApp.Controllers
{
    public abstract class BaseController : Controller
    {
        public JsonResult ErrorMessage(string prmObjectAddedName, string msg)
        {
            return Json(new
            {
                objectAddedName = "",
                valid = false,
                Message = msg
            });
        }

        public JsonResult SuccessMessage(string prmObjectAddedName, string msg, string customFunction = "")
        {
            return Json(new
            {
                objectAddedName = prmObjectAddedName,
                valid = true,
                Message = msg,
                CustomFunction = customFunction
            });
        }

        public void SetTempMessage(string message)
        {
            TempData["SuccessMessage"] = message;
        }

        public void SetTempErrorMessage(string message)
        {
            TempData["ErrorMessage"] = message;
        }
    }
}