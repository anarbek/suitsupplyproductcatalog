﻿using Newtonsoft.Json;
using ProductCatalogCommon.BLL;
using ProductCatalogCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using AttributeRouting.Web.Http;
using System.Web.Http.Description;
using ProductCatalogMVCApp.Models.DataTables;

namespace ProductCatalogMVCApp.Controllers
{
    public class ProductSearchController : ApiController
    {
        /// <summary>
        /// Get products requested by datatable and provide sorting, searching capabilities
        /// </summary>
        /// <param name="request"></param>
        /// <returns>List of products in format, suitable for datatable.js</returns>
        [System.Web.Http.HttpGet]
        [AttributeRouting.Web.Http.HttpRoute("api/Products")]
        public DataTableResponse GetProducts(DataTableRequest request)
        {
            // Query products
            ProductBLL productRepository = new ProductBLL();
            var products = productRepository.GetAllProducts();

            // Searching Data
            IEnumerable<Product> filteredProducts;
            if (request.Search.Value != null && request.Search.Value != "")
            {
                var searchText = request.Search.Value.Trim();

                filteredProducts = products.Where(p =>
                        p.Name.Contains(searchText) ||
                        p.Code.Contains(searchText) ||
                        p.Price.ToString().Contains(searchText));
            }
            else
            {
                filteredProducts = products;
            }

            // Sort Data
            if (request.Order.Any())
            {
                int sortColumnIndex = request.Order[0].Column;
                string sortDirection = request.Order[0].Dir;

                Func<Product, string> orderingFunctionString = null;
                Func<Product, int> orderingFunctionInt = null;
                Func<Product, decimal?> orderingFunctionDecimal = null;
                Func<Product, DateTime?> orderingFunctionDateTime = null;

                switch (sortColumnIndex)
                {
                    case 0:     // Product ID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // Product Code
                        {
                            orderingFunctionString = (c => c.Code);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                    case 2:     // Product Name
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                    case 3:     //Price
                        {
                            orderingFunctionDecimal = (c => c.Price);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionDecimal)
                                    : filteredProducts.OrderByDescending(orderingFunctionDecimal);
                            break;
                        }
                    case 5:     //Last Updated
                        {
                            orderingFunctionDateTime = (c => c.LastUpdated);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionDateTime)
                                    : filteredProducts.OrderByDescending(orderingFunctionDateTime);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(request.Start).Take(request.Length);

            return new DataTableResponse
            {
                draw = request.Draw,
                recordsTotal = products.Count(),
                recordsFiltered = products.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };
        }
    }
}
