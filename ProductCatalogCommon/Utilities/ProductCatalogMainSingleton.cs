﻿using ProductCatalogCommon.Service.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Utilities
{
    public class ProductCatalogMainSingleton
    {
        #region Singleton Declarations
        private static volatile ProductCatalogMainSingleton _instance;
        private static object syncRoot = new Object();

        public static ProductCatalogMainSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new ProductCatalogMainSingleton();
                        }
                    }
                }

                return _instance;
            }
        } 
        #endregion

        #region Properties
        private string _connectionString;
        public string ConnectionString
        {
            get { return _connectionString; }            
        }

        private IProductService _productService;
        public IProductService ProductService
        {
            get { return _productService; }            
        } 
        #endregion

        #region Methods
        public void SetConnectionString(string prmConnectionString)
        {
            _connectionString = prmConnectionString;
        }

        public void SetProductService(IProductService prmProductService) {
            _productService = prmProductService;
        }
        #endregion
    }
}
