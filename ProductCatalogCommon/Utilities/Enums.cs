﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Utilities
{
    public class Enums
    {
        /// <summary>
        /// Object Create or Update Enum
        /// </summary>
        public enum enmObjectCreateUpdateStatus { 
            /// <summary>
            /// return if database operation is succesful
            /// </summary>
            Success = 1,
            /// <summary>
            /// return this if primary key value is negative or 0 
            /// </summary>
            ErrorWrongPrimaryKey = 2,
            /// <summary>
            /// return if code is duplicated in database
            /// </summary>
            ErrorCodeDuplicate = 3,   
            /// <summary>
            /// return this if price is 0 or negative
            /// </summary>
            ErrorWrongPrice = 4,
            /// <summary>
            /// return this if object is not found in db
            /// </summary>
            ErrorObjectNotFound = 5,
            /// <summary>
            /// return this if something wrong happened on database side
            /// </summary>
            ErrorDBRelated = 6,
            /// <summary>
            /// return this for any unhandled error
            /// </summary>
            ErrorOther = 7
        }

        public enum enmFileUploadResult {
            /// <summary>
            /// return if file upload operation is succesful
            /// </summary>
            Success = 1,
            /// <summary>
            /// return if file size is not valid
            /// </summary>
            ErrorFileSize = 2,
            /// <summary>
            /// return if file extension is not valid
            /// </summary>
            ErrorFileExtension = 3,
            /// <summary>
            /// return if file extension is not valid
            /// </summary>
            ErrorFileUpload = 4,
        }
    }
}
