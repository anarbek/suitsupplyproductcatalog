﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Utilities
{
    public class Constants
    {
        public static decimal PriceToBeConfirmed = 999;
        public static decimal MaximumFileUploadSize = 3000;
    }
}
