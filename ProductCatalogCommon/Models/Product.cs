﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Models
{
    public class Product
    {
        public int Id { get; set; }
        
        /// <summary>
        /// Product code, required
        /// </summary>
        [Required(ErrorMessage = "Product code is required")]
        [Display(Name = "Product code")]
        public string Code { get; set; }

        /// <summary>
        /// Product name, required
        /// </summary>
        [Required(ErrorMessage = "Product name is required")]
        [Display(Name = "Product name")]
        public string Name { get; set; }

        /// <summary>
        /// Product price, required
        /// </summary>
        [Required(ErrorMessage = "Product price is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Price should be bigger than 0")]
        public decimal? Price { get; set; }

        /// <summary>
        /// Product Photo
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// Product Last Update Date (Date and time)
        /// </summary>
        public DateTime? LastUpdated { get; set; }
    }
}
