﻿using ProductCatalogCommon.Models;
using ProductCatalogCommon.Service.ServiceInterfaces;
using ProductCatalogCommon.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.BLL
{
    public class ProductBLL
    {
        private IProductService productService
        {
            get
            {
                return ProductCatalogCommon.Utilities.ProductCatalogMainSingleton.Instance.ProductService;
            }
        }

        public List<Product> GetAllProducts()
        {
            return productService.GetAllProducts();
        }

        public Product GetProduct(int id)
        {
            return productService.GetProduct(id);
        }

        public Product GetProductByCode(string code)
        {
            return productService.GetProductByCode(code);
        }

        public List<Product> GetProductsByCode(string code)
        {
            return productService.GetProductsByCode(code);
        }

        public Product GetProductByName(string name)
        {
            return productService.GetProductByName(name);
        }

        public Enums.enmObjectCreateUpdateStatus Insert(Product prmObjectToInsert)
        {
            if (prmObjectToInsert.Price <= 0) return Enums.enmObjectCreateUpdateStatus.ErrorWrongPrice;
            Product productByCode = productService.GetProductByCode(prmObjectToInsert.Code);
            if (productByCode != null) return Enums.enmObjectCreateUpdateStatus.ErrorCodeDuplicate;
            prmObjectToInsert.LastUpdated = DateTime.Now;
            productService.Insert(prmObjectToInsert);
            if (prmObjectToInsert.Id <= 0) return Enums.enmObjectCreateUpdateStatus.ErrorDBRelated;
            return Enums.enmObjectCreateUpdateStatus.Success;
        }

        public Enums.enmObjectCreateUpdateStatus Update(Product prmObjectToUpdate)
        {
            if (prmObjectToUpdate.Id <= 0) return Enums.enmObjectCreateUpdateStatus.ErrorWrongPrimaryKey;
            if (prmObjectToUpdate.Price <= 0) return Enums.enmObjectCreateUpdateStatus.ErrorWrongPrice;

            Product oldProductFromDB = productService.GetProduct(prmObjectToUpdate.Id);
            if (oldProductFromDB == null) {
                return Enums.enmObjectCreateUpdateStatus.ErrorWrongPrimaryKey;
            }

            if (prmObjectToUpdate.Photo == null) {
                prmObjectToUpdate.Photo = oldProductFromDB.Photo;
            }

            prmObjectToUpdate.LastUpdated = DateTime.Now;
            Product oldProductWithThisCode = productService.GetProductByCode(prmObjectToUpdate.Code);
            if (oldProductWithThisCode != null && oldProductWithThisCode.Id != prmObjectToUpdate.Id)
            {
                return Enums.enmObjectCreateUpdateStatus.ErrorCodeDuplicate;
            }
            int updateResult = productService.Update(prmObjectToUpdate);
            if (updateResult <= 0) return Enums.enmObjectCreateUpdateStatus.ErrorDBRelated;
            return Enums.enmObjectCreateUpdateStatus.Success;
        }

        public int Delete(Product prmObjectToDelete)
        {
            if (prmObjectToDelete.Id <= 0) return 0;
            return productService.Delete(prmObjectToDelete);
        }
    }
}
