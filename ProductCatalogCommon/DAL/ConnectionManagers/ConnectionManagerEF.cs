﻿using ProductCatalogCommon.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.DAL.ConnectionManagers
{
    public class ConnectionManagerEF
    {
        public static ProductCatalogDBEntities CurrentDBContext {
            get {
                var db = new ProductCatalogCommon.DAL.ProductCatalogDBEntities(ProductCatalogMainSingleton.Instance.ConnectionString);
                return db;
            }
        }
    }
}
