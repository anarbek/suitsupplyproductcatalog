﻿using ProductCatalogCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Service.ServiceInterfaces
{
    public interface IProductService
    {
        List<Product> GetAllProducts();
        Product GetProduct(int id);
        Product GetProductByCode(string code);
        Product GetProductByName(string name);
        void Insert(Product prmObjectToInsert);
        int Update(Product prmObjectToUpdate);
        int Delete(Product prmObjectToDelete);

        List<Product> GetProductsByCode(string code);
    }
}
