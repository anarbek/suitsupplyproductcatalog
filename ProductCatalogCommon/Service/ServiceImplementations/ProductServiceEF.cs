﻿using ProductCatalogCommon.DAL;
using ProductCatalogCommon.DAL.ConnectionManagers;
using ProductCatalogCommon.Models;
using ProductCatalogCommon.Service.ServiceInterfaces;
using ProductCatalogCommon.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalogCommon.Service.ServiceImplementations
{
    public class ProductServiceEF : IProductService
    {
        public List<Models.Product> GetAllProducts()
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                return (from t in db.tPRODUCTs
                        select new Product()
                        {
                            Id = t.Id,
                            Code = t.Code,
                            Name = t.Name,
                            Price = t.Price,
                            Photo = t.Photo,
                            LastUpdated = t.LastUpdated
                        }).ToList();
            }
        }

        public List<Product> GetProductsByCode(string code)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                return (from t in db.tPRODUCTs.Where(t => t.Code == code)
                        select new Product()
                        {
                            Id = t.Id,
                            Code = t.Code,
                            Name = t.Name,
                            Price = t.Price,
                            Photo = t.Photo,
                            LastUpdated = t.LastUpdated
                        }).ToList();
            }
        }

        public Models.Product GetProduct(int id)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT productFromDB = FindProductFromDB(db, id);
                if (productFromDB == null) return null;
                Product result = MapObjectFromDB(productFromDB);
                return result;
            }
        }

        private tPRODUCT FindProductFromDB(ProductCatalogDBEntities db, int id)
        {
            return db.tPRODUCTs.FirstOrDefault(t => t.Id == id);
        }

        public Models.Product GetProductByCode(string code)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT productFromDB = db.tPRODUCTs.OrderByDescending(t => t.Id).FirstOrDefault(t => t.Code == code);
                if (productFromDB == null) return null;
                Product result = MapObjectFromDB(productFromDB);
                return result;
            }
        }

        public Models.Product GetProductByName(string name)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT productFromDB = db.tPRODUCTs.OrderByDescending(t => t.Id).FirstOrDefault(t => t.Name == name);
                if (productFromDB == null) return null;
                Product result = MapObjectFromDB(productFromDB);
                return result;
            }
        }

        public void Insert(Models.Product prmObjectToInsert)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT itemToSendToDB = new tPRODUCT();
                itemToSendToDB.Code = prmObjectToInsert.Code;
                itemToSendToDB.Name = prmObjectToInsert.Name;
                itemToSendToDB.Price = prmObjectToInsert.Price;
                itemToSendToDB.Photo = prmObjectToInsert.Photo;
                itemToSendToDB.LastUpdated = prmObjectToInsert.LastUpdated;
                db.tPRODUCTs.Add(itemToSendToDB);
                int result = db.SaveChanges();
                prmObjectToInsert.Id = itemToSendToDB.Id;
            }
        }

        public int Update(Models.Product prmObjectToUpdate)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT itemToSendToDB = FindProductFromDB(db, prmObjectToUpdate.Id);
                if (itemToSendToDB == null) return 0;
                itemToSendToDB.Code = prmObjectToUpdate.Code;
                itemToSendToDB.Name = prmObjectToUpdate.Name;
                itemToSendToDB.Price = prmObjectToUpdate.Price;
                itemToSendToDB.Photo = prmObjectToUpdate.Photo;
                itemToSendToDB.LastUpdated = prmObjectToUpdate.LastUpdated;
                return db.SaveChanges();
            }
        }

        public int Delete(Models.Product prmObjectToDelete)
        {
            using (var db = ConnectionManagerEF.CurrentDBContext)
            {
                tPRODUCT itemToDeleteFromDB = FindProductFromDB(db, prmObjectToDelete.Id);
                if (itemToDeleteFromDB == null) return 0;
                db.tPRODUCTs.Remove(itemToDeleteFromDB);
                return db.SaveChanges();
            }
        }

        private static Product MapObjectFromDB(tPRODUCT productFromDB)
        {
            Product result = new Product();
            result.Id = productFromDB.Id;
            result.Code = productFromDB.Code;
            result.Name = productFromDB.Name;
            result.Price = productFromDB.Price;
            result.Photo = productFromDB.Photo;
            result.LastUpdated = productFromDB.LastUpdated;
            return result;
        }
    }
}
