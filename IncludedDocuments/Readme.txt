Steps to get project built and run:
1) Create Database with name : "ProductCatalogDB" (name can be chosen different if you want)
2) Execute script located at "Database\ProductCatalogDB.sql" with DatabaseName: ProductCatalogDB
3) Change ConnectionString in web.config of web application project(ProductCatalogMVCApp), located at: "<connectionStrings>" section, to your restored DBName 
4) If necessary, install packages listed in packages.config file of web application project(ProductCatalogMVCApp)
5) Rebuild and run

To get unit tests built and run:
1) Change ConnectionString in app.config of UnitTest project(ProductCatalogTests), located at: "<connectionStrings>" section, to your restored DBName 
2) Rebuild test project
3) Run tests

Manually downloaded libraries are located within folder "CustomLibrariesJS"

Other notes:
Libraries used for design and javascript tools:
- https://startbootstrap.com/
- http://bootboxjs.com/
- https://www.datatables.net

Project was built using VisualStudio 2012
MVC Framework used: MVC4
Swagger was added for api documentation