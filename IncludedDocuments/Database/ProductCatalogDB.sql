USE [ProductCatalogDB]
GO
/****** Object:  Table [dbo].[tPRODUCT]    Script Date: 04.05.2018 15:23:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPRODUCT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[Photo] [nvarchar](200) NULL,
	[Price] [decimal](15, 3) NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_tPRODUCT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
