﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductCatalogCommon.Utilities;
using ProductCatalogCommon.Service.ServiceImplementations;
using ProductCatalogCommon.Models;
using ProductCatalogCommon.BLL;
using System.Collections.Generic;

namespace ProductCatalogTests
{
    [TestClass]
    public class ProductTests
    {
        [TestInitialize]
        public void Setup()
        {
            ProductCatalogMainSingleton.Instance.SetConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings["ProductCatalogDBEntities"].ConnectionString);
            ProductCatalogMainSingleton.Instance.SetProductService(new ProductServiceEF());
        }

        [TestMethod]
        public void TestProductCRUD()
        {
            ProductBLL dbProduct = new ProductBLL();
            #region Initial Count Check
            List<Product> listOfProductsBeforeTests = dbProduct.GetAllProducts();
            //Here we get count of product records in database to check later with
            int listOfProductsBeforeTestsCount = listOfProductsBeforeTests.Count; 
            #endregion
            string code = "testProductForTests";
            string name = "Test Product For Tests";

            #region Insert Test
            Product productToAdd = new Product();
            productToAdd.Code = code;
            productToAdd.Name = name;
            productToAdd.LastUpdated = DateTime.Now;

            #region Price Tests
            //Price must be greater than 0, code doesn't throw exception, but it is possible to detect that 
            //no insert was made when price is 0
            productToAdd.Price = 0;
            Enums.enmObjectCreateUpdateStatus createOrEditResult = dbProduct.Insert(productToAdd);
            Assert.IsTrue(createOrEditResult == Enums.enmObjectCreateUpdateStatus.ErrorWrongPrice);

            //With price greater than 0, code should work as expected and return Id of inserted Product
            productToAdd.Price = 10;
            createOrEditResult = dbProduct.Insert(productToAdd);
            Assert.IsTrue(createOrEditResult == Enums.enmObjectCreateUpdateStatus.Success);
            #endregion
            #endregion
            int idProductToTest = productToAdd.Id;

            #region Select Tests
            List<Product> listOfProducts = dbProduct.GetAllProducts();
            int countOfProducts = listOfProducts.Count;
            Assert.IsTrue(countOfProducts > 0);

            Product productByCode = dbProduct.GetProductByCode(code);
            Assert.IsNotNull(productByCode);
            Assert.AreEqual(code, productByCode.Code);

            Product productByName = dbProduct.GetProductByCode(code);
            Assert.IsNotNull(productByName);
            Assert.AreEqual(name, productByName.Name); 
            #endregion

            #region Update Test
            Product productToTestUpdateAndDelete = dbProduct.GetProduct(idProductToTest);
            Assert.IsNotNull(productToTestUpdateAndDelete);
            Assert.AreEqual(productToTestUpdateAndDelete.Code, productToAdd.Code);
            Assert.AreEqual(productToTestUpdateAndDelete.Price, productToAdd.Price);

            string codeForUpdate = "TestProductChangedForTest";
            productToTestUpdateAndDelete.Code = codeForUpdate;

            #region Price Tests
            //Price must be greater than 0, code doesn't throw exception, but it is possible to detect that 
            //no update was made when price is 0
            productToTestUpdateAndDelete.Price = 0;
            createOrEditResult = dbProduct.Update(productToTestUpdateAndDelete);
            Assert.IsTrue(createOrEditResult == Enums.enmObjectCreateUpdateStatus.ErrorWrongPrice);

            //With price greater than 0, code should work as expected and return 1 if update is successfull
            decimal priceToCheck = 12;
            productToTestUpdateAndDelete.Price = priceToCheck;
            createOrEditResult = dbProduct.Update(productToTestUpdateAndDelete);
            Assert.IsTrue(createOrEditResult == Enums.enmObjectCreateUpdateStatus.Success);
            #endregion

            Product productFromDB = dbProduct.GetProduct(idProductToTest);
            Assert.IsNotNull(productFromDB);
            Assert.AreEqual(productFromDB.Code, codeForUpdate);
            Assert.AreEqual(productFromDB.Price, priceToCheck); 
            #endregion

            #region Delete Test
            int deleteResult = dbProduct.Delete(productToTestUpdateAndDelete);
            Assert.IsTrue(deleteResult > 0); 
            #endregion

            #region Final Count Check
            //After delete code worked, final count of products in DB should be as in the beginning of Product CRUD tests
            List<Product> listOfProductsAfterTests = dbProduct.GetAllProducts();
            Assert.AreEqual(listOfProductsBeforeTestsCount, listOfProductsAfterTests.Count); 
            #endregion
        }
    }
}
